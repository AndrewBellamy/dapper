# LDAP Authentication

## Description:
Authenticates a given user against specified role access, using an LDAP provider.

### Tags:
LDAP, Authentication

### Properties:
In the list below, the names of required properties appear in bold. Any other properties (not in bold) are considered optional. The table also indicates any default values, and whether a property supports the [NiFi Expression Language](https://nifi.apache.org/docs/nifi-docs/html/expression-language-guide.html).

| Display Name | API Name | Default Value | Allowable Values | Description |
| ------------ | -------- | ------------- | ---------------- | ----------- |
| **LDAP Provider URL** | PROVIDER_URL |  |  | The URL of the LDAP provider. Example: *ldap://ldaphost:389*. |
| **Security Level** | SECURITY_AUTHENTICATION |  | * none<br>* simple<br>* strong | Specifies the security level to use; from *none*, *simple*, *strong*. If unspecified, the behavior is determined by the LDAP service provider. |
| **Bind DN** | SECURITY_PRINCIPAL |  |  | The Distinguished Name of the principal, for which the processor will authenticate to your LDAP service provider. |
| **Bind Credentials** | SECURITY_CREDENTIALS |  |  | The password for the Bind Distinguished Name. |
| **Base DN** | BASE_DN |  |  | The base Distinguished Name on which to search for usernames (CN). |
| **Role Attribute** | ROLE_ATTRIBUTE |  |  | An LDAP attribute in which user roles are organized. These can sometimes be unique to the LDAP Schema. |
| **Group(s)** | GROUP_S |  |  | A semi-colon separated list of groups to authenticate membership. At least one is required. |

### Relationships:

| Name | Description |
| ---- | ----------- |
| success | An authenticated request will route to this Success relationship. |
| failure | An unauthenticated request will route to this Failure relationship. |

### Reads Attributes:

| Name | Description |
| ---- | ----------- |
| common.name | The common name (username) for which to authenticate. |

### Writes Attributes:

None specified.

### State management:

This component does not store state.

### Restricted:
This component is not restricted.

### Input requirement:

This component requires an incoming relationship.

### System Resource Considerations:

None specified.