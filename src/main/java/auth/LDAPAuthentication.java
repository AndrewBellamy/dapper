package auth;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

import javax.naming.directory.SearchControls;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchResult;

import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;

import org.apache.nifi.components.PropertyDescriptor;

import org.apache.nifi.expression.ExpressionLanguageScope;

import org.apache.nifi.logging.ComponentLog;

import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.util.StandardValidators;

import org.apache.nifi.processor.exception.ProcessException;

import org.apache.nifi.flowfile.FlowFile;

@Tags({ "ldap", "authentication" })
@CapabilityDescription("Authenticates a given user against specified role access, using an LDAP provider.")
@ReadsAttributes({
    @ReadsAttribute(attribute = "common.name", description = "The common name (username) for which to authenticate.")
})
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
public class LDAPAuthentication extends AbstractProcessor {

    private static final String INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
    private static final String REFERRAL = "follow";
    private static final Integer QUEUE_GRAB = 50;

    public static final PropertyDescriptor PROVIDER_URL = new PropertyDescriptor.Builder().name("LDAP Provider URL")
            .description("The URL of the LDAP provider. Example: 'ldap://ldaphost:389'")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor SECURITY_AUTHENTICATION = new PropertyDescriptor.Builder()
            .name("Security Level")
            .description(
                    "Specifies the security level to use; from 'none', 'simple', 'strong'. If unspecified, the behavior is determined by the LDAP service provider")
            .required(false)
            .allowableValues("none", "simple", "strong").build();

    public static final PropertyDescriptor SECURITY_PRINCIPAL = new PropertyDescriptor.Builder().name("Bind DN")
            .description(
                    "The Distinguished Name of the principal, for which the processor will authenticate to your LDAP service provider.")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor SECURITY_CREDENTIALS = new PropertyDescriptor.Builder()
            .name("Bind Credentials").description("The password for the Bind Distinguished Name.")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor BASE_DN = new PropertyDescriptor.Builder().name("Base DN")
            .description("The base Distinguished Name on which to search for usernames (CN).")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor ROLE_ATTRIBUTE = new PropertyDescriptor.Builder().name("Role Attribute")
            .description("An LDAP attribute in which user roles are organized. These can sometimes be unique to the LDAP Schema.")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final PropertyDescriptor GROUP_S = new PropertyDescriptor.Builder().name("Group(s)")
            .description("A semi-colon separated list of groups to authenticate membership. At least one is required.")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .build();

    public static final Relationship SUCCESS = new Relationship.Builder().name("success")
            .description("An authenticated request will route to this Success relationship.").build();

    public static final Relationship FAILURE = new Relationship.Builder().name("failure")
            .description("An unauthenticated request will route to this Failure relationship.").build();

    private List<PropertyDescriptor> properties;
    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> initProperties = new ArrayList<>();
        initProperties.add(PROVIDER_URL);
        initProperties.add(SECURITY_AUTHENTICATION);
        initProperties.add(SECURITY_PRINCIPAL);
        initProperties.add(SECURITY_CREDENTIALS);
        initProperties.add(BASE_DN);
        initProperties.add(ROLE_ATTRIBUTE);
        initProperties.add(GROUP_S);
        properties = Collections.unmodifiableList(initProperties);

        final Set<Relationship> initRelationships = new HashSet<>();
        initRelationships.add(SUCCESS);
        initRelationships.add(FAILURE);
        relationships = Collections.unmodifiableSet(initRelationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relationships;
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return properties;
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        final List<FlowFile> flowFiles = session.get(QUEUE_GRAB);

        final ComponentLog logger = getLogger();
        
        Hashtable<String, String> environment = new Hashtable<>();
        environment.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
        environment.put(Context.REFERRAL, REFERRAL);
        environment.put(Context.PROVIDER_URL, context.getProperty(PROVIDER_URL).toString());
        environment.put(Context.SECURITY_AUTHENTICATION, context.getProperty(SECURITY_AUTHENTICATION).toString());
        environment.put(Context.SECURITY_PRINCIPAL, context.getProperty(SECURITY_PRINCIPAL).toString());
        environment.put(Context.SECURITY_CREDENTIALS, context.getProperty(SECURITY_CREDENTIALS).toString());

        try {
            DirContext boundContext = new InitialDirContext(environment);

            for (FlowFile flowFile : flowFiles) {
                String commonName = flowFile.getAttribute("common.name");
                if (commonName == null) {
                    logger.info("Missing required attribute 'common.name'. Routing to Failure.");
                    session.transfer(flowFile, FAILURE);
                    session.getProvenanceReporter().route(flowFile, FAILURE);
                    continue;
                }

                String filter = "(CN=" + commonName.toUpperCase() + ")";

                String[] attributeIds = {context.getProperty(ROLE_ATTRIBUTE).toString()};
                SearchControls searchControls = new SearchControls();
                searchControls.setReturningAttributes(attributeIds);
                searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

                NamingEnumeration<SearchResult> searchResults = boundContext
                    .search(context.getProperty(BASE_DN).toString(), filter, searchControls);
                session.getProvenanceReporter().invokeRemoteProcess(
                    flowFile, 
                    context.getProperty(PROVIDER_URL).toString(), 
                    "Searched user " + commonName + ", on BaseDN " + context.getProperty(BASE_DN).toString() + ", retrieving memberOf attribute."
                );

                if (searchResults.hasMore()) {
                    logger.debug("LDAP result found for CN: " + commonName);
                    SearchResult result = searchResults.next();
                    Attributes attributes = result.getAttributes();
                    if (containsAllGroups(
                            attributes.get(context.getProperty(ROLE_ATTRIBUTE).toString()).toString(), 
                            context.getProperty(GROUP_S).toString().split(";"))
                    ) {
                        session.transfer(flowFile, SUCCESS);
                        session.getProvenanceReporter().route(flowFile, SUCCESS);
                    } else {
                        logger.info(
                            "Membership in specified groups ("
                             + context.getProperty(GROUP_S).toString()
                             + ") could not be found. Routing to Failure.");
                        session.transfer(flowFile, FAILURE);
                        session.getProvenanceReporter().route(flowFile, FAILURE);
                    }
                } else {
                    logger.info("LDAP result, for CN: " + commonName + ", was not found. Routing to Failure.");
                    session.transfer(flowFile, FAILURE);
                    session.getProvenanceReporter().route(flowFile, FAILURE);
                }
            }

            boundContext.close();
        } catch (NamingException err) {
            logger.error("Processing error occurred: " + err.getMessage());
            throw new ProcessException(err.getMessage());
        }
    }

    private static boolean containsAllGroups(String memberOf, String... groups) {
        for (String group : groups) {
            if (!memberOf.contains(group)) {
                return false;
            }
        }
        return true;
    }
}
