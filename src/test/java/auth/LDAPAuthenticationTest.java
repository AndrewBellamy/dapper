package auth;

import org.junit.runner.RunWith;
import org.junit.Test;
import java.util.HashMap;

import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.apache.nifi.reporting.InitializationException;

import org.apache.directory.server.core.annotations.ApplyLdifFiles;
import org.apache.directory.server.core.annotations.CreateDS;
import org.apache.directory.server.core.annotations.CreatePartition;

import org.apache.directory.server.core.integ.FrameworkRunner;
import org.apache.directory.server.core.integ.AbstractLdapTestUnit;

import org.apache.directory.server.annotations.CreateLdapServer;
import org.apache.directory.server.annotations.CreateTransport;

@RunWith(FrameworkRunner.class)
@CreateLdapServer(transports = { @CreateTransport(protocol = "LDAP", address = "localhost", port = 10389) })
@CreateDS(
    allowAnonAccess = false, 
    partitions = { @CreatePartition(name = "Internal", suffix = "DC=Internal,DC=local")}
)
@ApplyLdifFiles(value = {"users.ldif"})
public class LDAPAuthenticationTest extends AbstractLdapTestUnit{

    private static TestRunner testRunner;
    private static final String PROVIDER_URL = "ldap://localhost:10389";
    private static final String SECURITY_AUTHENTICATION = "simple";
    private static final String SECURITY_PRINCIPAL = "CN=Admin,DC=Internal,DC=local";
    private static final String SECURITY_CREDENTIALS = "secret";
    private static final String BASE_DN = "DC=Internal,DC=local";
    /*We're using seeAlso attribute to test memberOf or groupMembership which aren't LDAP standard.*/
    private static final String ROLE_ATTRIBUTE = "seeAlso";
    private static final String GROUP_S = "CN=Test-group,OU=Groups,DC=Internal,DC=local; CN=Test-comp-group,OU=Groups,DC=Internal,DC=local;";

    private static void init(boolean wrong) throws InitializationException {
        testRunner = TestRunners.newTestRunner(LDAPAuthentication.class);

        testRunner.setProperty(LDAPAuthentication.PROVIDER_URL, PROVIDER_URL);
        testRunner.setProperty(LDAPAuthentication.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION);
        testRunner.setProperty(LDAPAuthentication.SECURITY_PRINCIPAL, SECURITY_PRINCIPAL);
        testRunner.setProperty(LDAPAuthentication.SECURITY_CREDENTIALS, SECURITY_CREDENTIALS);
        testRunner.setProperty(LDAPAuthentication.BASE_DN, wrong ? "BRUM-BRUM-BRUM" : BASE_DN);
        testRunner.setProperty(LDAPAuthentication.ROLE_ATTRIBUTE, ROLE_ATTRIBUTE);
        testRunner.setProperty(LDAPAuthentication.GROUP_S, GROUP_S);
    }

    @Test
    public void arePropertiesValid() throws Exception {
        init(false);

        testRunner.assertValid();
    }

    @Test
    public void testSuccessFlow() throws Exception {
        init(false);

        HashMap<String, String> attr = new HashMap<String, String>();
        attr.put("common.name", "Test-user");
        testRunner.enqueue("data", attr);

        testRunner.run();

        testRunner.assertAllFlowFilesTransferred(LDAPAuthentication.SUCCESS);
    }

    @Test
    public void testFailFlow() throws Exception {
        init(false);

        HashMap<String, String> attributePatterns = new HashMap<String, String>();
        attributePatterns.put("common.name", "Test-non-user");
        attributePatterns.put("common.name", "Test-luser");
        attributePatterns.put("common.blame", "Test-luser");

        attributePatterns.forEach((key, value) -> {
            HashMap<String, String> attr = new HashMap<String, String>();
            attr.put(key, value);
            testRunner.enqueue("data", attr);
        });

        testRunner.run();

        testRunner.assertAllFlowFilesTransferred(LDAPAuthentication.FAILURE);
    }

    @Test
    public void testUserWithSingleRoleFailure() throws Exception {
        init(false);

        HashMap<String, String> attributePatterns = new HashMap<String, String>();
        attributePatterns.put("common.name", "Test-comp-user");

        testRunner.enqueue("data", attributePatterns);

        testRunner.run();

        testRunner.assertAllFlowFilesTransferred(LDAPAuthentication.FAILURE);
    }
}
